# Draw Window
-Options to choose width and length in inches,<br>
    as well as an image to display through the window.<br>
-Window border is always normalized to 3 inches,<br>
    may appear smaller or larger based on window dimensions

<img src="https://thumbs.gfycat.com/HollowGiantKoodoo.webp" width="300" height="250" />
